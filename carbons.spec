Name:           carbons
Version:        0.2.3
Release:        1%{?dist}
Summary:        Experimental XEP0280 plugin for libpurple

License:        GPLv2
URL:            https://github.com/gkdr/carbons
Source0:        https://github.com/gkdr/carbons/archive/v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  pkgconfig(purple)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(libxml-2.0)

%description
Experimental XEP0280 plugin for libpurple

%global debug_package %{nil}

%prep
%autosetup -p1 -n carbons-%{version}

%build
make %{?_smp_mflags}

%install
%make_install

%files
%{_libdir}/purple-2/*
%doc README.md

%changelog
%autochangelog
